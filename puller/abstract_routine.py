from datetime import datetime

__author__ = 'dcortes'

import ConfigParser
import logging
import os
from cloghandler import ConcurrentRotatingFileHandler as RotatingFileHandler
from driver_management.mongo_management import mongo_management
from driver_management.http_management import http_management
from parser.loss_coefficient_parser import LossCoefficientParser
from dateutil.relativedelta import relativedelta


class abstract_routine():

    def __init__(self):
        self.config = self._get_config()
        self._get_logger()
        self.loss_parser = LossCoefficientParser(self.logger)
        self.manager = mongo_management()

    def _get_logger(self):
        self.logger = logging.getLogger(self.name)
        if len(self.logger.handlers) == 0:
            if self.config["logger_level"] == 'info':
                level = logging.INFO
            else:
                level = logging.DEBUG
            self.logger.setLevel(level)
            self.handler = RotatingFileHandler(self.config['logger_path'], maxBytes=204800, backupCount=10)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def _get_config(self):
        if not os.environ.has_key("SETTINGS_PATH_PULLER"):
            os.environ["SETTINGS_PATH_PULLER"] = "/etc/dexma/wsm_puller.ini"
        print os.environ["SETTINGS_PATH_PULLER"]
        config_dict = dict()
        config = ConfigParser.RawConfigParser()
        config.read(os.environ["SETTINGS_PATH_PULLER"])
        config_dict["host"] = config.get("esios", "host")
        config_dict["token"] = config.get("esios", "token")
        config_dict["concepts"] = config.get("esios", "concepts").split()
        config_dict["concepts_loss"] = config.get("esios", "concepts_loss").split()
        config_dict["logger_path"] = config.get("logger", "path")
        config_dict["logger_level"] = config.get("logger", "level")
        self.name = config.get("app", "name")
        return config_dict

    def _get_concepts_ids(self):
        #we get the collections that are tue ids of the parameters + prices and the system.indexes and we erase both
        # in this way we don't need to remember each time that a parameter id change to modify every place a file
        """collections = self.manager.get_collections()
        collections.remove("prices")
        collections.remove("system.indexes")
        return collections"""
        return self.config["concepts"]

    def _get_concepts_loss_ids(self):
        return self.config["concepts_loss"]

    def _get_concept_readings(self, concept, from_date, to_date):
        headers = {"Authorization": 'Token token ="%s"' % self.config["token"]}
        url = "/indicators/%s" % concept
        parameters = {
                        "start_date": from_date.strftime("%Y-%m-%d"),
                        "end_date": to_date.strftime("%Y-%m-%dT%H:%M"),
                        "time_trunc": "hour",   # possible values "ten minutes", "hour", "day"m "month", "year"
                        #   "time_agg": "sum", sum is the default not needed
                        #   "geo_agg": "sum", not needed as sum is the default
                        #   "geo_ids": None,
                        "locale": "es"
                      }
        return http_management.call_rest_get(self.config["host"], url, self.logger, params=parameters, headers=headers)

    def _get_concept_readings_zips(self, from_date, to_date, type='3', date_type="datos"):
        url = "/archives/%s/download?date_type=%s&start_date=%s&end_date=%s" \
              %(type, date_type, from_date.strftime("%Y-%m-%d"), to_date.strftime("%Y-%m-%d"))
        return http_management.call_rest_get(self.config["host"], url, self.logger, response='no_json')

    def _format_readings(self, reads):
        values = list()
        for read in reads:
            new_val = {"v": (read["value"])/1000, "ts": read["datetime"]}
            values.append(new_val)
        return values

    def _insert_concept(self, concept, reads, from_date):
        pass

    def _insert_perd_concepts(self, concepts, from_date, to_date):
        pass

    def _exists_concept(self, concept, name):
        if name.find(concept) != -1 and (name.find('perd2') != -1 or name.find('perd3') != -1 or name.find('perdg') != -1):
            return True
        return False

    def _get_concept_file_A1(self, concept, month):
        end_date = (month.replace(day=1) + relativedelta(months=1, days=-1)).strftime("%Y%m%d")
        if concept == "20DH":
            concept = "20D"
        if concept[0] == "6":
            concept = "g" + concept
        return "A1_perd{}_{}_{}".format(concept, month.replace(day=1).strftime("%Y%m%d"), end_date)

    def launch_tasks(self, consolidation):
        pass

    def general_launch(self, from_date, to_date, tasks=True):
        self.logger.info('Getting Concepts id')
        concepts = self._get_concepts_ids()
        self.logger.info('Inserting Prices from all different Concepts register')
        consolidation = self._insert_concepts(concepts, from_date, to_date)
        if tasks:
            self.logger.info('Enqueue TASKS To celery ')
            self.launch_tasks(consolidation, from_date, to_date)

    def general_loss_launch(self, from_date, to_date, tasks=True):
        self.logger.info('Inserting Prices from all different Concepts register')
        concepts = self._get_concepts_loss_ids()
        consolidation = self._insert_perd_concepts(concepts, from_date, to_date)
        if tasks:
            self.logger.info('Enqueue TASKS To celery ')
            self.launch_tasks(consolidation, from_date, to_date)

    def launch_insertion(self):
        pass

