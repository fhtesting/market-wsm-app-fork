__author__ = 'dcortes'


from dateutil.relativedelta import relativedelta
import zipfile, StringIO
from abstract_routine import abstract_routine

class DemandRoutine(abstract_routine):

    def __init__(self):
        """
        This routine will add the new values for the future day on mongo without checking
        :return:
        """
        abstract_routine.__init__(self)

    def _insert_concept(self, concept, reads, from_date):
        """
        First we need to look if the document is already on the collection if exists we just
        need to push the new values to the list values and sort them, if not we need to create
        the document
        # we use each and not pushall as the second one si just enable from 2.4 to future versions
        :param concept: the collection key
        :param reads: the reads to be append to the values key of the document
        :return:
        """
        query = {"month": from_date.strftime("%m-%Y")}
        if self.manager.get_document_exists(concept, query):
            new_doc = {"month": from_date.strftime("%m-%Y"),"values": reads}
            self.manager.replace_object(concept, new_doc, query)
        else:
            query["values"] = reads
            self.manager.insert_object(concept, query)

    def _insert_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": True}
        for concept in concepts:
            try:
                concept = int(concept)
                reads = self._get_concept_readings(concept, from_date, to_date)
                if reads is not None and len(reads.get("indicator", {}).get("values", [])) > 0 :
                    reads = self._format_readings(reads["indicator"]["values"])
                    self._insert_concept(str(concept)+'s', reads, from_date)
                else:
                    self.logger.warning("Problem taking data for concept %s for inteval [%s, %s]" % (concept,
                                                                                from_date.strftime("%m:%d"),
                                                                                to_date.strftime("%m:%d")
                                                                                ))
            except:
                self.logger.exception("not true concept")
                consolidation["status"] = False
        return consolidation

    def _insert_perd_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False}
        zip_files = ""
        try:

            zip_files = self._get_concept_readings_zips(from_date, to_date, type='8')
            if "No values for specified archive" in zip_files:
                zip_files = self._get_concept_readings_zips(from_date, to_date, type='3')

            with zipfile.ZipFile(StringIO.StringIO(zip_files), 'r') as zfile:
                name_list = zfile.namelist()
                for name in name_list:
                    if len(name.split('_')) < 5:
                        for concept in concepts:
                            if self._exists_concept(concept, name.split('_')[1]):
                                zfiledata = zfile.read(name)
                                reads = self.loss_parser._get_list_of_prices(zfiledata, from_date)
                                self._insert_concept(concept+'s', reads, from_date)
                                consolidation["status"] = True
        except:
            self.logger.exception("Problem parsing zip file %s" %zip_files)

        return consolidation

    def launch_insertion(self, from_date, to_date):
        while from_date <= to_date:
            self.general_launch(from_date, from_date + relativedelta(months=1), tasks=False)
            from_date += relativedelta(months=1)

    def launch_loss_insertion(self, from_date, to_date):
        while from_date <= to_date:
            self.general_loss_launch(from_date, from_date, tasks=False)
            from_date += relativedelta(months=1)

