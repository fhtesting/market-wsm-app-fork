__author__ = 'dcortes'

from mockito import mock, when
from datetime import datetime
import os
from datetime import timedelta, date

from testing_conf import conf_path_puller, conf_path_driver, esios_response
from puller.daily_routine import DailyRoutine


class TestDaily():

    def setup_method(self, method):
        os.environ["SETTINGS_PULLER_PATH"] = conf_path_puller
        os.environ["SETTINGS_DRIVERS_PATH"] = conf_path_driver

    def test_get_concepts_readings(self):
        dr = DailyRoutine()
        from_date = date.today() + timedelta(days=1)
        to_date = from_date + timedelta(days=1, seconds=-1)
        result = dr._get_concept_readings(805, from_date, to_date)
        assert(result["indicator"]["short_name"] == "Mercado Diario")

    def test_format_readings(self):
        dr = DailyRoutine()
        reads = dr._format_readings(esios_response["indicator"]["values"])
        print reads[0]
        assert(reads[0] == {'ts': '2015-08-05T00:00:00.000+02:00', 'v': 63.48}
)