import csv
import pytz
import logging
from datetime import timedelta
from StringIO import StringIO

class LossCoefficientParser():

    def __init__(self, logger):
        self.logger = logger


    def __set_logger(self, config):
        logger = logging.getLogger(config['mac'])
        self.logger = logger


    def __getTuples_from_string(self, stream):
        lines = []
        reader = csv.reader(StringIO(stream.replace('\r', '')), delimiter=';')
        for row in reader:
            lines.append(row)
        return lines


    def _get_list_of_prices(self, file_stream, from_date):
        list_of_prices = []
        try:
            lines = self.__getTuples_from_string(file_stream)
            for line in lines[2:]:
                if len(line) > 1:
                    day_prices = self.get_prices_from_day(line, from_date)
                    list_of_prices.extend(day_prices)
        except:
            self.logger.exception('Problem getting prices from file')
        return list_of_prices

    def get_prices_from_day(self, line, from_date):
        list_price_day = list()
        day = line[0].split(' ')[-1]
        date = from_date.replace(day=int(day))
        for index, price in enumerate(line[1:]):
            if len(price) > 0:
                map = {
                                        'ts': self._get_timestamp(date, index),
                                        'v': float(price)/100
                      }
                list_price_day.append(map)
        return list_price_day



    def _get_timestamp(self, date, index):
        timezone = pytz.timezone('Europe/Madrid')
        naivedate = date + timedelta(hours=index)
        timezonedate = timezone.localize(naivedate)
        return timezonedate.astimezone(timezone).isoformat()
