__author__ = 'dcortes'

from datetime import date, timedelta, datetime, time
from dateutil.relativedelta import relativedelta
from celery_engine.client import Client
from abstract_routine import abstract_routine
import zipfile, StringIO


class MonthlyRoutine(abstract_routine):

    def __init__(self):
        abstract_routine.__init__(self)

    def get_hashed_reads(self, reads, key, value, factor):
        values = dict()
        for read in reads:
            values[read[key]] = read[value] * factor
        return values

    def _consolidation(self, reads_new, reads_old):
        """
        If some read differs from the old version saved on our bd then that means that the previous calculations need to be
        redone
        """
        for ts, read in reads_new.items():
            read = "%.4f" % read
            old_read = "%.4f" % reads_old.get(ts, 0)
            if old_read != read:
                return True
        return False

    def _insert_concept(self, concept, reads, from_date):
        """
        First we need to look if the document is already on the collection if exists we just
        need to push the new values to the list values and sort them, if not we need to create
        the document
        # we use each and not pushall as the second one si just enable from 2.4 to future versions
        :param concept: the collection key
        :param reads: the reads to be append to the values key of the document
        :return:
        """
        query = {"month": from_date.strftime("%m-%Y")}
        if self.manager.get_document_exists(concept, query):
            new_doc = {"month": from_date.strftime("%m-%Y"),"values": reads}
            self.manager.replace_object(concept, new_doc, query)
        else:
            query["values"] = reads
            self.manager.insert_object(concept, query)


    def _get_consolidation_info(self, reads, hash_reads, concept, from_date, consolidation):
        reads_saved = self.manager.find_object(str(concept)+'s', {"month": from_date.strftime("%m-%Y")})

        if reads_saved is None or len(reads_saved.get("values",{})) == 0:
            self.logger.debug('There is not readings saved in mongo from concept %s' %concept)
            hash_reads_saved = {}
        else:
            hash_reads_saved = self.get_hashed_reads(reads_saved["values"], "ts", "v", 1)

        if self._consolidation(hash_reads, hash_reads_saved):
            self.logger.debug('Doing Consolidation from concept %s' %str(concept))
            self._insert_concept(str(concept)+'s', reads, from_date)
            consolidation["status"] = True
            consolidation["concepts"].append(concept)
        else:
            self.logger.debug('There is not Consolidation from concept %s' %str(concept))

        return consolidation

    def _insert_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False, "concepts": []}
        for concept in concepts:
            try:
                self.logger.debug('Get information from Concept %s' % str(concept))
                concept = int(concept)
                reads = self._get_concept_readings(concept, from_date, to_date)
                if reads is not None and len(reads.get("indicator", {}).get("values", [])) > 0:
                    hash_reads = self.get_hashed_reads(reads["indicator"]["values"], "datetime", "value", 0.001)
                    reads = self._format_readings(reads["indicator"]["values"])
                    consolidation = self._get_consolidation_info(reads, hash_reads, concept, from_date, consolidation)
                else:
                    self.logger.warning("Problem taking data for concept %s for inteval [%s, %s]" % (concept,
                                                                                from_date.strftime("%m:%d"),
                                                                                to_date.strftime("%m:%d")
                                                                                ))
            except:
                self.logger.exception("not true concept")
        return consolidation

    def _insert_perd_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False, "concepts": []}
        zip_files = ""
        try:
            zip_files = self._get_concept_readings_zips(from_date, to_date, type='8')
            with zipfile.ZipFile(StringIO.StringIO(zip_files), 'r') as zfile:
                for name in zfile.namelist():
                    if len(name.split('_')) < 5:
                        for concept in concepts:
                            if self._exists_concept(concept, name.split('_')[1]):
                                zfiledata = zfile.read(name)
                                reads = self.loss_parser._get_list_of_prices(zfiledata, from_date)
                                hash_reads = self.get_hashed_reads(reads, 'ts', 'v', 1)
                                consolidation = self._get_consolidation_info(reads, hash_reads, concept, from_date, consolidation)
        except:
            self.logger.exception("Problem parsing zip file %s" %zip_files)

        return consolidation

    def launch_tasks(self, consolidation, from_date, to_date):
        if consolidation["status"]:
            concepts = consolidation["concepts"]
            prices = self.manager.find_objects("prices", search_args={}, query_args={"dep_id": 1, "price": 1,"formula": 1, "_id": 0})
            for price in prices:
                if any(substring in concepts for substring in price.get("formula", "")):
                    Client.enqueue("celery_engine.tasks.calculate_price", args=[price["dep_id"], price["price"], from_date, to_date, 3,'consolidation'])

    def launch_insertion(self):
        from_date = date.today().replace(day=1) - relativedelta(months=1)
        to_date = date.today().replace(day=1) - relativedelta(seconds=1)
        self.logger.info('Starting Launch Monthly Price Consolidation From %s to %s ' % (from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')))
        self.general_launch(from_date, to_date)
        self.logger.info('Monthly consolidation Done')

    def launch_loss_insertion(self):
        date_var = date.today().replace(day=1) - relativedelta(months=1)
        from_date = datetime(date_var.year, date_var.month, date_var.day)
        to_date = date.today().replace(day=1) - relativedelta(seconds=1)
        self.logger.info('Starting Launch  Daily Loss Price insertion From %s to %s ' % (from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')) )
        self.general_loss_launch(from_date, to_date)
        self.logger.info('Daily insertion of task Done')

if __name__ == '__main__':
    dr = MonthlyRoutine()
    dr.launch_insertion()
    dr.launch_loss_insertion()