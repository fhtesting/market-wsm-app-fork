__author__ = 'dcortes'

import uuid
uuid._uuid_generate_random = None

from datetime import timedelta, date, datetime, time
from dateutil.relativedelta import relativedelta
from abstract_routine import abstract_routine
from celery_engine.client import Client
import zipfile, StringIO


import os
if not os.environ.has_key("SETTINGS_PATH_PULLER"):
    os.environ["SETTINGS_PATH_PULLER"] = "/etc/dexma/wsm_puller.ini"

class DailyRoutine(abstract_routine):

    def __init__(self):
        """
        This routine will add the new values for the future day on mongo without checking
        :return:
        """
        abstract_routine.__init__(self)

    def _insert_concept(self, concept, reads, from_date):
        """
        First we need to look if the document is already on the collection if exists we just
        need to push the new values to the list values and sort them, if not we need to create
        the document
        # we use each and not pushall as the second one si just enable from 2.4 to future versions
        :param concept: the collection key
        :param reads: the reads to be append to the values key of the document
        :return:
        """
        query = {"month": from_date.strftime("%m-%Y")}
        concept = str(concept) + "s"
        if self.manager.get_document_exists(concept, query):
            new_fields = {"$push": {"values": {"$each": reads, "$sort": {"ts": 1}}}}
            self.manager.update_object(concept, new_fields, query)
        else:
            query["values"] = reads
            self.manager.insert_object(concept, query)

    def _insert_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False}
        for concept in concepts:
            try:
                concept = int(concept)
                reads = self._get_concept_readings(concept, from_date, to_date)
                if reads is not None and len(reads.get("indicator", {}).get("values", [])) > 0:
                    reads = self._format_readings(reads["indicator"]["values"])
                    self._insert_concept(concept, reads, from_date)
                    consolidation["status"] = True
                else:
                    self.logger.warning("Problem taking data for concept %s for inteval [%s, %s]" % (concept,
                                                                                from_date.strftime("%m:%d"),
                                                                                to_date.strftime("%m:%d")
                                                                                ))

            except:
                self.logger.exception("not true concept")

        return consolidation

    def _insert_perd_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False}
        zip_files = ""
        try:
            zip_files = self._get_concept_readings_zips(from_date, to_date, type='3')
            with zipfile.ZipFile(StringIO.StringIO(zip_files), 'r') as zfile:
                for name in zfile.namelist():
                    if len(name.split('_')) < 5:
                        for concept in concepts:
                            if self._exists_concept(concept, name.split('_')[1]):
                                zfiledata = zfile.read(name)
                                reads = self.loss_parser._get_list_of_prices(zfiledata, from_date)
                                self._insert_concept_historical(concept, reads, from_date)
                                consolidation["status"] = True
        except:
            self.logger.exception("Problem parsing zip file %s" %zip_files)

        return consolidation

    def _insert_concept_historical(self, concept, reads, from_date):
        query = {"month": from_date.strftime("%m-%Y")}
        concept = str(concept) + "s"

        document_to_replace = {
            "values": reads,
            "month": query["month"]
        }
        self.manager.replace_object(concept, document_to_replace, query)

    def _insert_perd_concepts_historical(self, concepts, from_date):
        zip_files = ""
        folder_searched = "A1_liquicomun_{}.zip".format(from_date.strftime("%Y%m"))
        try:
            start_date = from_date - relativedelta(years=1)
            zip_files = self._get_concept_readings_zips(start_date, from_date, type='2', date_type="publicacion")
            with zipfile.ZipFile(StringIO.StringIO(zip_files), 'r') as zfile:
                for name in zfile.namelist():
                    if name == folder_searched:
                        month_files = zfile.read(name)
                        with zipfile.ZipFile(StringIO.StringIO(month_files), 'r') as month_files:

                            for concept in concepts:
                                try:
                                    concept_file = self._get_concept_file_A1(concept, from_date)
                                    zfiledata = month_files.read(concept_file)
                                    reads = self.loss_parser._get_list_of_prices(zfiledata, from_date)
                                    self._insert_concept_historical(concept, reads, from_date)
                                except:
                                    self.logger.exception("Problem parsing concept%s in file %s" % (concept, zip_files))
        except Exception as e:
            self.logger.exception("Problem parsing zip file %s" %zip_files)

    def launch_tasks(self, consolidation, from_date, to_date):
        if consolidation["status"]:
            prices = self.manager.find_objects("prices", search_args={}, query_args={"_id":0})
            for price in prices:
                self.logger.info('Enqueue task with priceid = %s, depid = %s, from date = %s , to date = %s' %(str(price["dep_id"]),price['price'], from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')))
                Client.enqueue("celery_engine.tasks.calculate_price", args=[price["dep_id"], price["price"], from_date, to_date, 3,'importation'])

    def launch_insertion(self):
        tomorrow = date.today() + timedelta(days=1)
        from_date = datetime.combine(tomorrow, time.min)
        to_date = datetime.combine(tomorrow, time.max)
        self.logger.info('Starting Launch  Daily Price insertion From %s to %s ' % (from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')) )
        self.general_launch(from_date, to_date)
        self.logger.info('Daily insertion of task Done')

    def launch_loss_insertion(self):
        today = date.today()
        from_date = datetime.combine(today, time.min)
        to_date = datetime.combine(today, time.max)
        self.logger.info('Starting Launch  Daily Loss Price insertion From %s to %s ' % (from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')))
        self.general_loss_launch(from_date, to_date)
        self.logger.info('Daily insertion of task Done')

    def launch_loss_insertion_next_month(self):
        today = date.today().replace(day=1) + relativedelta(months=1)
        from_date = datetime.combine(today, time.min)
        self.logger.info('Starting Launch  Nexto month Daily Loss Price insertion From %s' % (from_date.strftime('%d/%m/%Y %H:%M:%S')))
        concepts = self._get_concepts_loss_ids()
        self._insert_perd_concepts_historical(concepts, from_date)
        self.logger.info('Daily insertion of task Future values Done')


if __name__ == '__main__':
    dr = DailyRoutine()
    dr.launch_insertion()
    dr.launch_loss_insertion()
    dr.launch_loss_insertion_next_month()

