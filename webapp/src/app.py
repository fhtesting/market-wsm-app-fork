__author__ = 'dcortes'

# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, g
import locale
import datetime
from flask.ext.babel import Babel


from views.setup import mod_setup
from logger.app_logger import logger_invocation
from views.wsm_view import mod_wsm
from views.wsm_config_price_view import mod_wsm_config_price
from views.wsm_list_prices_view import mod_wsm_list_price
from views.price_api_calls import mod_wsm_api_call
from driver_management.redis_managements import redis_management
from driver_management.mongo_management import mongo_management
from driver_management.http_management import http_management
import os
import ConfigParser

# For import *
__all__ = ['create_app']


def create_app(config="SETTINGS_PATH", app_name='wsm_app'):
    app = Flask(app_name)
    configure_app(app, config)
    configure_logging(app)
    configure_hook(app)
    configure_blueprints(app)
    configure_jinja_filters(app)
    configure_extensions(app)
    configure_error_handlers(app)
    app.logger.info("all configuration set it perfectly")
    app.logger.info(app.url_map)
    app.logger.info(app.root_path)
    return app


def get_managers():
    return {
            "redis": redis_management(),
            "mongo": mongo_management(),
            "http": http_management()
            }

def get_security_settings(app, config):
    app.config["THREADS_PER_PAGE"] = config.getint("security", "threads_per_page")
    app.config["CSRF_ENABLED"] = config.getboolean("security", "csrf_enabled")
    app.config["CSRF_SESSION_KEY"] = config.get("security", "csrf_session_key")
    app.config["SECRET_KEY"] = config.get("security", "secret_key")

def get_webapp_settings(app, config):
    app.config["NAME"] = config.get("app", "name")
    app.config["ID"] = config.get("app", "id")
    app.config["SECRET"] = config.get("app", "secret")
    app.config["ACCEPT_LANGUAGES"] = config.get("app", "accept_languages").split(",")
    app.config["DEBUG"] = config.getboolean("app", "debug")

def get_dexcell_settings(app, config):
    app.config["DEXCELL_ENDPOINT"] = config.get("dexcell", "endpoint")


def get_logger_settings(app, config):
    app.config["LOGGER_FILE_FORMAT"] = config.get("logger", "file_format")


def configure_app(app, config_path):
    if not os.environ.has_key(config_path):
        os.environ[config_path] = "/etc/dexma/wsm_webapp.ini"
    config = ConfigParser.RawConfigParser()
    config.read(os.environ[config_path])
    get_security_settings(app, config)
    get_webapp_settings(app, config)
    get_dexcell_settings(app, config)
    get_logger_settings(app, config)
    app.config["managers"] = get_managers()


def configure_extensions(app):
    babel = Babel(app)
    @babel.localeselector
    def get_locale():
        return g.get('current_lang', 'en')


def configure_blueprints(app):
    """Configure blueprints in views."""
    app.register_blueprint(mod_wsm, url_prefix='/', template_folder='templates')
    app.register_blueprint(mod_wsm_config_price, url_prefix='/price_config', template_folder='templates')
    app.register_blueprint(mod_setup, url_prefix='/setup', template_folder='templates')
    app.register_blueprint(mod_wsm_list_price, url_prefix='/list_price', template_folder='templates')
    app.register_blueprint(mod_wsm_api_call, url_prefix='/price', template_folder='templates')

def configure_logging(app):
    """Configure file(info) and email(error) logging."""
    logger_invocation.obtain_logger(app)


def configure_jinja_filters(app):
    @app.template_filter('number')
    def _number_filter(number, decimals=0):
        locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')
        if number != -1:
            try:
                if decimals == 0:
                    return locale.format(u'%d', round(number), True)
                else:
                    return locale.format(u'%.2f', round(number, decimals), True)
            except:
                return '--'
        else:
            return '--'


def configure_hook(app):
    @app.after_request
    def add_header(response):
        response.cache_control.max_age = 300
        expiry_time = datetime.datetime.utcnow() + datetime.timedelta(100)
        last_modify = datetime.datetime.strptime("19/05/2015 13:26:00", "%d/%m/%Y %H:%M:%S")
        response.headers["Cache-Control"] = "private,Public"
        response.headers["Last-Modified"] = last_modify.strftime("%a, %d %b %Y %H:%M:%S GMT")
        response.headers["Expires"] = expiry_time.strftime("%a, %d %b %Y %H:%M:%S GMT")
        return response
    @app.before_request
    def before_request():
        pass


def configure_error_handlers(app):

    @app.errorhandler(401)
    def forbidden_page(error):
        return render_template("errors/401.html"), 401

    @app.errorhandler(404)
    def not_found(error):
        return render_template('errors/404.html'), 404

    @app.errorhandler(500)
    def server_error_page(error):
        return render_template("errors/500.html"), 500
