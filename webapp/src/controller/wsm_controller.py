__author__ = 'dcortes'

from ..models.prices_model import pricesModel
from ..models.token_model import token_model
from ..models.dexcell_prices import dexcellPrices
from ..models.dexcell_session import dexcellSession
from celery_engine.client import Client
from datetime import datetime

class wsm_controller():

    def __init__(self, dep_id, managers, config):
        self.dep_id = dep_id
        self.managers = managers
        self.config = config
        self.token = token_model(managers["redis"]).get_token(config['NAME'],dep_id)

    def get_language(self, session_hash):
        dexcell_session = dexcellSession(self.managers["http"], self.token, self.config["NAME"], self.config["DEXCELL_ENDPOINT"])
        session = dexcell_session.get_dexcell_session(session_hash)
        return session.get("user", {}).get("locale", "en_EN").split("_")[0] if type(session) == dict else "en"

    def _get_wsm_prices(self, dexcell_prices):
        wsm_prices = list()
        price_dexcell_json = {}
        if dexcell_prices is not None:
            for price in dexcell_prices:
                if price.get("consumption", {}).get("type", "TOU") == "TOU_WSM": #this line should be change by TOU-WSM
                    price_dexcell_json = {}
                    price_dexcell_json['id'] = price['id']
                    price_dexcell_json['name'] = price['name']
                    price_dexcell_json['num_of_periods'] = price['num_of_periods']
                    price_dexcell_json['tariff_structure'] = price['tariff_structure']['id']
                    wsm_prices.append(price_dexcell_json)
        return wsm_prices

    def get_total_prices(self):
        prices = pricesModel(self.dep_id, self.managers['mongo'])
        prices_number = prices.get_prices_count()
        return prices_number

    def get_prices_from_dexcell(self):
        #Getting prices from apiv3
        dexcell_prices = dexcellPrices(self.managers["http"], self.token, self.config["NAME"], self.config["DEXCELL_ENDPOINT"])
        prices_dexcell = dexcell_prices.get_dexcell_prices()
        return self._get_wsm_prices(prices_dexcell)

    def get_prices(self, page_number,start_price):
        #Getting configured prices from data base
        prices = pricesModel(self.dep_id, self.managers['mongo'])
        skip_documents = (page_number-1) * start_price
        prices_wsm_list = prices.get_configured_prices(skip_documents,start_price)
        return prices_wsm_list

    def delete_price_config(self, dep_id, price_id):
        #Delete price configuration
        prices = pricesModel(self.dep_id, self.managers['mongo'])
        status = prices.delete_prices(price_id)
        return status

    def recalculate_price(self,depid,price,from_date,to_date):

            from_date_datetime = datetime.strptime(from_date,"%d/%m/%Y")
            to_date_datetime = datetime.strptime(to_date,"%d/%m/%Y")
            Client.enqueue("celery_engine.tasks.calculate_price", args=[depid, price, from_date_datetime, to_date_datetime, 3,''])
            return 'OK'

