__author__ = 'dcortes'

from ..models.token_model import token_model


class wsm_config_price():

    def __init__(self, dep_id, managers, config):
        self.dep_id = dep_id
        self.managers = managers
        self.token = token_model(managers["redis"]).get_token(config['NAME'],dep_id)
        self.collection_price = "prices"
        self.bd_manager = managers["mongo"]

    def insert_price(self, price_document):
        doc_id = self.bd_manager.insert_object(self.collection_price, price_document)
        return "ok" if doc_id is not None else "bad"

    def update_price(self, price_document, price_old):
        searcher = {"dep_id": price_document["dep_id"], "price": price_old}
        return self.bd_manager.replace_object(self.collection_price, price_document, searcher)

    def get_price_config(self,dep_id, price_id):
        searcher = {"dep_id": dep_id, "price": price_id}
        return self.bd_manager.find_object(self.collection_price, searcher)

    def exist_price(self, dep_id, price):
        searcher = {"dep_id": dep_id, "price": price}
        return self.bd_manager.get_document_exists(self.collection_price, searcher)
