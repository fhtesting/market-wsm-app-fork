__author__ = 'dcortes'


class token_model():

    def __init__(self, redis_app):
        self._db = redis_app

    def save_token(self, app_name, dep_id, perm_token):
        """
        For saving the new token for the client and the app
        :param app_name: the application name under what all his tokens are saved
        :param dep_id: the deployment identifier to take his applications tokens
        :param perm_token: the token for the api v3 to be used for the app app_name for the deployment with dep_token
        :return:
        """
        try:
            return self._db.insert_object(dep_id,"perm_token", perm_token, dumpsState=False)
        except:
            return False

    def get_token(self, app_name, dep_id):
        """
        Returns the json with the configuration saved for the location
        :param app_name: the application name under what all his tokens are saved
        :param dep_token: the deployment specific token to take his applications tokens
        :return:
        """
        try:
            return self._db.find_object(dep_id,"perm_token", loads_cond=False)
        except:
            return -1
