__author__ = 'dcortes'

import logging
from logging import NullHandler
from logging import NullHandler,StreamHandler
import sys
from driver_management.http_management import http_management

class DRapi_oauth():

    def __init__(self, endpoint, app_id, secret, logger_name):
        self.endpoint = endpoint
        self.id = app_id
        self.secret = secret
        self._get_logger()

    def _get_logger(self):
        self.logger = logging.getLogger()
        if len(self.logger.handlers) == 0:
            self.logger.setLevel(logging.INFO)
            self.handler = StreamHandler(sys.stdout)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def get_oauth_token(self, temp_token):
        url = "/oauth/access-token?app_id=%s&secret=%s&temp_token=%s" % (self.id, self.secret, temp_token)
        return http_management.call_rest_post(self.endpoint, url, self.logger, "str")