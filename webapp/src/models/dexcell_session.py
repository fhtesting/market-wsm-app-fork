__author__ = 'jnegrete'

import logging
from logging import NullHandler,StreamHandler
import sys


class dexcellSession():

    def __init__(self, http_management, token, logger_name, endpoint):
        self.http_management = http_management
        self.headers = {"x-dexcell-token": token}
        self._get_logger()
        self.endpoint = endpoint

    def _get_logger(self):
        self.logger = logging.getLogger()
        if len(self.logger.handlers) == 0:
            self.logger.setLevel(logging.INFO)
            self.handler = StreamHandler(sys.stdout)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def get_dexcell_session(self, session_hash):
        """
        this function get from Dexcell Api the temporal session to have the language
        token
        :return:
        """
        return self.http_management.call_rest_get(self.endpoint, "/session/%s" % session_hash, self.logger, headers=self.headers)
