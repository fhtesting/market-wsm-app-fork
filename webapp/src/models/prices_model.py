__author__ = 'jnegrete'


class pricesModel():

    def __init__(self, dep_id,db):
        self._dep_id = dep_id
        self.db = db

    def get_configured_prices(self,skip_doc,limit):
        """
        Get the prices configured by the app
        :return: An array of prices basic info dictionaries
        """
        query = {'dep_id':self._dep_id}
        prices_list = self.db.find_objects('prices', query, {"_id":0}, skip=skip_doc,limit=limit)
        return prices_list

    def get_price(self, price_id):
        """
        Get all the information for the specific price with id price_id from the deployment dep_id
        :return: A dictionary with all the information associated with the price
        """
        query = {'price':price_id}
        price = self.db.find_object('prices', query)
        return price

    def get_prices_count(self):
        '''
        Return The number of prices configured in this deployment
        :return: A string that indicate the number of prices
        '''
        query = {'dep_id':self._dep_id}
        prices_number = self.db.count_objects('prices', query, 0)
        return prices_number

    def delete_prices(self, price_id):
        '''
        Delete a price config by price id
        :param price_id:
        :return:
        '''
        query = {'dep_id':self._dep_id, 'price':price_id}
        status = self.db.delete_object('prices', query)
        return status