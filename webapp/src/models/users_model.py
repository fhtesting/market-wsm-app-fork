__author__ = 'dcortes'

from bson.json_util import loads, dumps

class users_model():

    def __init__(self, redis_app):
        self._redis = redis_app

    def save_new_user(self, app_name, dep_id):
        """
            We check if the deployment id exists in the list, if not we add it
        """
        try:
            list_users = self._redis.insert_object("deployments")
            list_users = loads(list_users) if list_users is not None else []
            if dep_id not in list_users:
                list_users.append(dep_id)
                self._redis.insert_object("deployments", dumps(list_users))
            return True
        except Exception as e:
            print e
            return False

    def get_user_list(self, app_name):
        """
        Returns the json with the configuration saved for the location
        :param app_name: the application name under what all his tokens are saved
        :param dep_token: the deployment specific token to take his applications tokens
        :return:
        """
        try:
            users = self._redis.find_object("app:%s:deployments" % app_name)
            return loads(users) if users is not None else []
        except:
            return []
