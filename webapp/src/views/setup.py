#!/usr/bin/python
# coding=utf-8

__author__ = 'dcortes'

from flask import Blueprint, current_app, redirect, request, render_template

from driver_management.redis_managements import redis_management
from ..models.dexcell_oauth import DRapi_oauth
from ..models.token_model import token_model

mod_setup = Blueprint('setup', __name__)


def render_bad_page(error, callback):
    render_html = render_template("errors/general_error.html", error=error, callback=callback)
    return render_html


@mod_setup.route('/')
def setup():
    """
    We use the temp_token from the URL to take the perm_token for the client
    After using the dep_token (it doesnt' change for the client) we save it on redis
    Ass this application save information in redis and we need to knwo how many deployments have it, every time one user
    register in the app he will be added into a list
    :return:
    """
    current_app.logger.info('installing virtual_app')
    try:
        callback = str(request.args.get("callback", "http://dexcell.com/"))
        DEXCELL_ENDPOINT = current_app.config['DEXCELL_ENDPOINT']
        ID = current_app.config['ID']
        SECRET = current_app.config['SECRET']
        LOGGER_NAME = current_app.config['NAME']
        dro = DRapi_oauth(DEXCELL_ENDPOINT, ID, SECRET, logger_name=LOGGER_NAME)
        dep_id = request.args.get("dep_id", "None")
        if dep_id != "None":
            temp_token = unicode(request.args.get("temp_token", "None"))
            perm_token = dro.get_oauth_token(temp_token)
            bd = redis_management()
            tok_model = token_model(bd)
            state = tok_model.save_token(LOGGER_NAME, dep_id, unicode(perm_token))
            if state is False:
                return render_bad_page("Any problem configuring the application", callback)
        else:
            return render_bad_page("Deployment token null", callback)
    except:
        current_app.logger.exception('problem with setup!!!')
        return render_bad_page("Unknow problem with setting the session", callback)
    return redirect(callback)