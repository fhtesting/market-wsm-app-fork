# -*- coding: utf-8 -*-
__author__ = 'dcortes'

from flask import Blueprint, render_template, current_app, jsonify, request, g
from math import ceil

from driver_management.redis_managements import redis_management
from driver_management.mongo_management import mongo_management
from driver_management.http_management import http_management
from ..controller.wsm_controller import wsm_controller

mod_wsm = Blueprint('wsm', __name__)


#@mod_wsm.before_request
def before_request():
    """
    Get the language to pass to babel flask to set the correct language in what show the templates:
    """
    dep_id = int(request.args.get('dep_id'))
    session_id = request.args.get('session_id')
    managers = current_app.config['managers']
    controller = wsm_controller(dep_id, managers, current_app.config)
    lang = controller.get_language(session_id)
    if lang not in current_app.config["ACCEPT_LANGUAGES"]:
        lang = "en"
    g.current_lang = lang


@mod_wsm.route('/', methods=['GET'])
def wsm_render():
    """
        The main call that return the list of prices created, and the current dexcell prices still available
    """
    try:
        current_app.logger.info('begining principal view')
        current_app.logger.info(current_app.root_path)
        before_request()
        dep_id = int(request.args.get('dep_id'))
        managers = current_app.config['managers']
        controller = wsm_controller(dep_id, managers, current_app.config)
        total_prices = controller.get_total_prices()
        prices_dexcell = controller.get_prices_from_dexcell()
        pages_prices = 20
        if total_prices == 0:
            num_pages = 1
        elif total_prices%pages_prices == 0:
            num_pages = total_prices/pages_prices
        else:
            num_pages = (total_prices/pages_prices) + 1

        render_html = render_template("wsm.html",
                                      deployment_id=dep_id,
                                      debug=current_app.config["DEBUG"],
                                      prices_dexcell=prices_dexcell,
                                      total_prices=total_prices,
                                      page_prices=pages_prices,
                                      num_pages=num_pages,
                                      selected_page=1,
                                      )
        return render_html
    except:
        current_app.logger.exception("Problem loading view")
        return render_template("errors/500.html")



@mod_wsm.route('ping', methods=['GET', 'POST'])
def virtual_device_ping():
    """
    A simple ping call to test that the app it's still up
    :return:
    """
    return "pong"


@mod_wsm.route('app-status', methods=['GET', 'POST'])
def virtual_device_status():
    """
    A more complex app status testing approx to be able to see if all dependencies are working correctly
    :return:
    """
    return jsonify(name=current_app.config["LOGGER_NAME"], status="alive")