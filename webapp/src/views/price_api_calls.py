from flask import Blueprint, render_template, current_app, request, jsonify
from datetime import datetime
mod_wsm_api_call = Blueprint('price_api_call', __name__)
from puller.demand_routine import DemandRoutine
from puller.daily_routine import DailyRoutine



@mod_wsm_api_call.route('/concepts', methods=['POST'])
def recalculate_price():

        from_date = str(request.args.get("from_date"))
        to_date = str(request.args.get("to_date"))
        routine = DemandRoutine()
        try:
            from_date_datetime = datetime.strptime(from_date, "%Y-%m")
            to_date_datetime = datetime.strptime(to_date, "%Y-%m")
        except:
            return "403 Bad Date Format, Example: %Y-%m-%d "
        routine.launch_insertion(from_date_datetime,to_date_datetime)
        routine.launch_loss_insertion(from_date_datetime,to_date_datetime)
        return 'OK'


@mod_wsm_api_call.route('/users', methods=['POST'])
def recalculate_users_cost():

        from_date = str(request.args.get("from_date"))
        to_date = str(request.args.get("to_date"))
        routine = DailyRoutine()
        try:
            from_date_datetime = datetime.strptime(from_date, "%Y-%m-%d")
            to_date_datetime = datetime.strptime(to_date, "%Y-%m-%d")
        except:
            return "403 Bad Date Format, Example: %Y-%m-%d "
        routine.launch_tasks({"status": True}, from_date_datetime, to_date_datetime)
        return 'OK'



