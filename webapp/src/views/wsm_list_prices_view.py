__author__ = 'jnegrete'
from flask import Blueprint, render_template, current_app, jsonify, request, g

mod_wsm_list_price = Blueprint('price_list', __name__)

from ..controller.wsm_controller import wsm_controller


@mod_wsm_list_price.route('/next_page', methods=['GET'])
def get_next():
    try:
        num_page = int(request.args.get("page_number"))
        dep_id = int(request.args.get("dep_id"))
        price_x_page = int(request.args.get("page_prices"))
        managers = current_app.config['managers']
        controller = wsm_controller(dep_id, managers, current_app.config)
        prices_wsm_conf = controller.get_prices(num_page, price_x_page)
        dexcell_prices = controller.get_prices_from_dexcell()
        prices_wsm = []
        for price_conf in prices_wsm_conf:
            for pos,dexcell_price in enumerate(dexcell_prices):
                if price_conf['price'] == dexcell_price['id']:
                    price_conf['price_name'] = dexcell_price['name']
                    prices_wsm.append(price_conf)
                    dexcell_prices.pop(pos)
                    break
        return jsonify(prices_wsm=prices_wsm)
    except:
        current_app.logger.exception("Problem loading price list")
        return render_template("errors/500.html")


@mod_wsm_list_price.route('/price/delete', methods=['POST'])
def wsm_delete_price():
    '''
    giving the information from one price configuration
    :return:
    '''

    try:
        dep_id = int(request.form.get("dep_id"))
        price_id = str(request.form.get("price_id"))
        managers = current_app.config['managers']
        controller = wsm_controller(dep_id, managers, current_app.config)
        controller.delete_price_config(dep_id, price_id)
        return jsonify(wsm_price='OK')
    except:
        current_app.logger.exception("Problem loading view")
        return render_template("errors/500.html")

@mod_wsm_list_price.route('/recalculate', methods=['POST'])
def wsm_recalculate_price():
    '''
    giving the information from one price configuration
    :return:
    '''

    try:
        dep_id = int(request.form.get("dep_id"))
        price_id = str(request.form.get("price_id"))
        from_date = str(request.form.get("from_date"))
        to_date = str(request.form.get("to_date"))
        managers = current_app.config['managers']
        controller = wsm_controller(dep_id, managers, current_app.config)
        wsm_price = controller.recalculate_price(dep_id, price_id,from_date,to_date)
        return jsonify(wsm_price=wsm_price)
    except:
        current_app.logger.exception("Problem loading view")
        return render_template("errors/500.html")