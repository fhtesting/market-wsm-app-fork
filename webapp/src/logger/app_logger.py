__author__ = 'dcortes'

## to obtain the configuration from the app
from flask import current_app

from logging.handlers import RotatingFileHandler
from logging import StreamHandler
from logging import Formatter
import logging
import os
import sys
from gmail_handler import TlsSMTPHandler

#from logutils.queue import QueueHandler, QueueListener
import Queue


class logger_invocation():

    @staticmethod
    def obtain_logger(app=current_app):
        LOGGER_FILE_FORMAT = app.config['LOGGER_FILE_FORMAT']
        app.logger.setLevel(logging.INFO)
        #app.logger.addHandler(logger_invocation._add_file_handler(LOGGER_NAME, PATH_LOG, LOGGER_FILE_FORMAT))
        #app.logger.addHandler(logger_invocation._add_mail_handler(ADMINS, FROM_MAIL, PASS_MAIL, LOGGER_NAME))
        app.logger.addHandler(logger_invocation._add_stream_handler(LOGGER_FILE_FORMAT))

    @staticmethod
    def _add_file_handler(LOGGER_NAME, PATH_LOG, LOGGER_FILE_FORMAT):
        pathLog = os.path.join(PATH_LOG, LOGGER_NAME)
        handler = RotatingFileHandler(os.path.join(pathLog, LOGGER_NAME + ".log"), maxBytes=10000000, backupCount=10)
        handler.setFormatter(logging.Formatter(LOGGER_FILE_FORMAT))
        return handler

    @staticmethod
    def _add_stream_handler(logger_file_format):
        handler2 = StreamHandler(sys.stdout)
        handler2.setFormatter(logging.Formatter(logger_file_format))
        return handler2

    @staticmethod
    def _add_mail_handler(admins, from_mail, pass_mail, LOGGER_NAME):
        mail_handler = TlsSMTPHandler(('smtp.gmail.com', 587),
                                      from_mail,
                                      admins, '%s Failed' % LOGGER_NAME,
                                      (from_mail, pass_mail))
        mail_handler.setFormatter(Formatter('''
                                    Message type:       %(levelname)s
                                    Location:           %(pathname)s:%(lineno)d
                                    Module:             %(module)s
                                    Function:           %(funcName)s
                                    Time:               %(asctime)s

                                    Message:

                                    %(message)s
                                                '''))
        mail_handler.setLevel(logging.ERROR)
        que = Queue.Queue(-1)
        queue_handler = QueueHandler(que)
        queue_handler.setLevel(logging.ERROR)
        listener = QueueListener(que, mail_handler)
        listener.start()
        return queue_handler