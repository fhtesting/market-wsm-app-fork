#!/bin/bash
pybabel extract -F babel.cfg -o messages.pot .

pybabel init -i messages.pot -d translations -l es
pybabel init -i messages.pot -d translations -l ca