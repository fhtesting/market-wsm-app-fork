/**
 * Created by jnegrete on 26/08/15.
 */

function initDXUI(){
    DX.ui.renderizeCheckboxes();
    DX.ui.renderizeDropdowns();
    DX.ui.renderizeRadios();
    DX.ui.preventHrefScroll();
}

function pre_config(){
    var $config_bone = $("#config-price-bone").clone();
    var $prices_bone = $("#prices-bone").clone();
    var $config_zone = $("#config-zone");
    $prices_bone.find("#not-used-prices-bone").attr("id", "not-used-prices");
    $config_bone.find("#prices-select").html($prices_bone.html());
    $config_zone.html($config_bone.html());
    $prices_bone.show();
    $config_bone.show();
    return $config_zone;
}
function build_price_form(interval_pos,json_data){
    var periods = $("#not-used-prices option:selected").data('periods');
    var interval = $('#interval').clone();
    var div = $('#intervals');
    div.append(interval);
    interval.show()
    var control_price = interval.find("#control-price")
    for ( var pos = 0; pos < periods; ++pos) {
        label = "<label class='header'>P" + (pos + 1).toString() +"</label>"
        id = 'intervalprice'+ interval_pos +'-price'+pos.toString()+''
        if (json_data.hasOwnProperty('P'+(pos + 1).toString())){
            val = (json_data['P'+(pos + 1).toString()]).toString()
        }
        else
        {
            val = ''
        }
        div_aux = '<div class="controls"> <input type="text" class="numeric" maxlength="8" value="'+ val +'" class="price_neg input-small" id='+id+'> </div>'
        control_price.append(label);
        control_price.append(div_aux);
    }
    separator_line = '<div class="separatorLine"></div>'
    interval.append(separator_line);
    interval.attr('id','interval' + interval_pos)
    if (interval_pos == 0)
    {
        interval.attr('name','')
    }
    else
    {
        interval.attr('name','interval-change-price')
    }
    return interval
}

function deleteInterval(interval_id){
    $('#' + interval_id).remove()
}

function addChangePriceForm(){
      add_change_price({})
}

function add_change_price(jsonObj){
    var form_number = $(".numberOfIntervals").attr('value')
    interval = build_price_form(form_number,jsonObj)
    var control_label= interval.find('#control-tittle')
    var delete_function = "deleteInterval('"+ interval.attr('id') +"')"
    control_label.append('<div class="controls"><span class="intervalTitle">Change of Price ' + form_number + '</span><span class="deleteInterval delete" onclick="'+ delete_function +'"></span></div>')
    var control_date = interval.find('#control-date')
    control_date.attr('style','')
    var date_picker = interval.find('#control-date .controls input')
    date_picker.attr('id','datepicker-interval'+form_number)
    if (jsonObj['date']){
        date_picker.attr('value',jsonObj['date'])
    }
    $(".numberOfIntervals").attr('value', (parseInt(form_number,10) + 1).toString())
    DX.ui.datepicker('datepicker-interval'+form_number);
    $(".numeric").numeric()
    $("#ui-datepicker-div").hide();
}
function buildOtherConceptsTable(){
     var table = $("#other-concepts-table tbody");
     table.html("");
     $json_other_cost.forEach(function(json){
         table.append(convertConceptToTD(json));
     });
     $('.numberOfVar').attr('value',($json_other_cost.length + 1).toString())
     DX.ui.renderizeCheckboxes();

}
function backToConfigPrice(){
   var $other_concepts_zone = $("#other-concepts-zone");
    $other_concepts_zone.hide()
    $("#config-zone").show()
    buildOtherConceptsTable()
    $(".numberOfIntervals").attr('value', '1')
    DX.ui.renderizeDropdowns();

}
function gettingPrices(interval){
    var concepts = {}
    interval.each(function(i,obj){
        var float_numbert = $('#'+obj.id).val()
        if (float_numbert.length == 0){
            concepts = {}
            return false
        }
        var key = 'P'+ (i + 1).toString()
        var value = parseFloat(float_numbert)
        concepts[key] = value
    });
    console.log(concepts)
    return concepts
}

function serializeOtherCostToJson(json_object,varid){
    var name = $("#concepts_price_name").val()
    if (name.length == 0)
    {
        return {}
    }
    var concepts = gettingPrices($("#interval0 .numeric"))
    if (jQuery.isEmptyObject(concepts))
    {
        return {}
    }
    json_object['name'] = name
    json_object['concepts'] = concepts
    json_object['var'] = varid
    var change_concepts = []
    $("div[name=interval-change-price]").each(function(i,elem){
        var intervalid = $('#'+elem.id);
        var date = intervalid.find('#control-date .controls input').val()
        concepts = gettingPrices($('#'+elem.id).find('.numeric'));
        if (jQuery.isEmptyObject(concepts) || date.length == 0 )
        {
            json_object = {}
            return false
        }
        concepts['date'] = date
        change_concepts.push(concepts)
    });
    if (jQuery.isEmptyObject(json_object))
    {
        return json_object
    }
    else
    {
         json_object['changeconcepts'] = change_concepts
        console.log(json_object)
        return json_object
    }

}

function saveOtherConcept(varid){
    var json_cost = serializeOtherCostToJson({},'v' + varid)
    console.log(json_cost)
    if (jQuery.isEmptyObject(json_cost)){
         DX.ui.notification($comments_dic['empty_fields_error'], "error");
    }
    else{
        $json_other_cost.push(json_cost)
        backToConfigPrice()
        $var.attr('value', (parseInt($var.attr('value'),10) +1).toString())

    }
}

function editJson(varid){
    var json_cost = serializeOtherCostToJson($json_config,varid)
     if (jQuery.isEmptyObject(json_cost)){
         DX.ui.notification($comments_dic['empty_fields_error'], "error");
    }
    else {
         $json_config = json_cost
         backToConfigPrice()
     }

}

function editOtherConcepts(varid){
    $json_config = getOtherConceptConfig(varid)
    preOtherConcepts()
    $("#concepts_price_name").val($json_config.name)
    build_price_form(0,$json_config.concepts)

    $json_config.changeconcepts.forEach(function(obj){
        add_change_price(obj)
    });

    var func = 'editJson("'+ varid +'")'
    $("#save-concept").attr("onclick", func);
    $(".numeric").numeric()
    DX.ui.renderizeDropdowns();
}

function getOtherConceptConfig(varid){
    var json_obj = {};
    $json_other_cost.forEach(function(obj){
       if (obj.var == varid){
           json_obj =  obj
       }
   });
    return json_obj;
}

function preOtherConcepts(){
    var $other_concepts = $("#other-concepts-form").clone();
    $other_concepts.show();
    var $other_concepts_zone = $("#other-concepts-zone");
    $other_concepts_zone.html($other_concepts.html());
    $("#config-zone").hide()
    $other_concepts_zone.show()
    $var = $('.numberOfVar')
}



function new_concept_price(){
    preOtherConcepts()
    build_price_form(0,{})
    var func = 'saveOtherConcept("'+ $var.attr('value') + '")'
    $("#save-concept").attr("onclick", func);
    $(".numeric").numeric()
    initDXUI();
}

function post_config($config_zone){
    $config_zone.show();
    $("#list-zone").hide();
    initDXUI();
    $('#not-used-prices').select2();
    $("#sites").select2();
    $("#loss_coefficients").select2();
}

function new_price_config(){
    $config_zone = pre_config();
    post_config($config_zone);
    $json_other_cost = []
    $(".save-button").attr("onclick", "save_price('new','-','-')");
    DX.ui.renderizeDropdowns();
}

function edit_price_config(price){
    $config_zone = pre_config();
    $config_zone.find("#formula-text").text(price.formula);
    $config_zone.find('#not-used-prices option[value="'+ price.price + '"]').prop("selected", true);
    post_config($config_zone);
    for (pos in price.concepts){
        $id = "#"+price.concepts[pos];
        if ($("#loss_coefficients option[value='"+price.concepts[pos]+"']").length > 0){
            $("#loss_coefficients").val(price.concepts[pos]).trigger('change');
            var label = $("#loss_coefficients").parent();
        }
        else{
            var label = $($id).parent();
        }
        label.addClass("checked");
    }
    $price_old = price.price;
    $json_other_cost = price.other_concepts
    buildOtherConceptsTable()
    $(".save-button").attr("onclick", "save_price('edit','"+ price.last_importation +"','"+ price.last_consolidation + "')");
    DX.ui.renderizeDropdowns();
}

function nice_save(price_text){
    $("#config-zone").css("opacity", 1);
    $("#config-zone").hide();
    $("#list-zone").show();
    load_page('1',num_pages)
    $("#formula-text").removeClass("error-text");
    //DX.ui.renderizeCheckboxes();
    //DX.ui.renderizeDropdowns();
    DX.ui.notification($comments_dic['save'], "success");
}

function validate_formula(formula, concepts,vars){
    var check_if_correct = true;
    String.prototype.contains = function(it){return this.indexOf(it) != -1;};
    for (i in concepts){
        var x = formula.contains(concepts[i]);
        if (x !== true){
            DX.ui.notification($comments_dic['validate_formula_error1'] +" "+ concepts[i] + " "+ $comments_dic['validate_formula_error2'] , "error");
            $("#config-zone").css("opacity", 1);
            $("#formula-text").addClass("error-text");
            check_if_correct = false
        }
        else{
            formula = formula.replace(concepts[i],'1')
        }
    }

    try
    {
        var y = eval(formula);
    }
    catch(e)
    {
        DX.ui.notification($comments_dic['validate_formula_error3'], "error");
        check_if_correct = false
    }
    console.log(y)
    return check_if_correct
}

function set_ui_status_before(){
    var $config_zone = $("#config-zone");
    var $ready_button = $("#ready-button");
    $config_zone.css("opacity", 0.8);
    $ready_button.addClass("working");
    $ready_button.text($comments_dic['saving_comments']);
}

function save_price(type,last_importation,last_consolidation){
    set_ui_status_before();
    var price = $("#not-used-prices option:selected").val();
    var periods = $("#not-used-prices option:selected").data('periods');
    var ts = $("#not-used-prices option:selected").data('ts');
    var formula = $("#formula-text").val();
    var concepts = [];
    var concepts_ids= [];
    var check_loss_coef = true;
    $('#concepts-zone .checked').each(function() {
        var input = $(this).find("input");
        concepts.push(input.attr('name'));
        if (input.attr('id') == 'loss_check'){
            var option = $( "#loss_coefficients option:selected").val();
            if (option == '-'){
                DX.ui.notification($comments_dic['loss_coef_no_selected'], "error");
                $("#config-zone").css("opacity", 1);
                $("#formula-text").addClass("error-text");
                check_loss_coef = false;
            }
            concepts_ids.push(option);
        }
        else{
            concepts_ids.push(input.attr('id'));
        }
    });
    var checker_formula = false;
    if (concepts.length !== 0){

        $json_other_cost.forEach(function(json){
         concepts.push(json.var);
     });
        checker_formula = validate_formula(formula, concepts);
    }
    else{
        DX.ui.notification($comments_dic['validate_concept_error'], "error");
        $("#config-zone").css("opacity", 1);
        $("#formula-text").addClass("error-text");
    }

    if (checker_formula && check_loss_coef){
        if (type == 'new'){
           new_price_ajax_call(concepts_ids,price,formula,periods,ts,last_importation,last_consolidation);
        }
        else if (type == 'edit'){
            edit_price_ajax_call(concepts_ids,price,formula,periods,ts,last_importation,last_consolidation);
        }
    }
    else{
        var $ready_button = $("#ready-button");
        $ready_button.text($comments_dic['save_button_message_error']);
        $ready_button.removeClass("working");
    }
}

function new_price_ajax_call(concepts_ids,price,formula,periods,ts,last_importation,last_consolidation){
        $.ajax({
            type: "POST",
            url: '/price_config/' + Pdep_id + "/new",
            cache:false,
            data :{concepts: concepts_ids, price: price, formula: formula, dep_id: Pdep_id, periods:periods, ts:ts, other_concepts:JSON.stringify($json_other_cost), last_importation: last_importation, last_consolidation: last_consolidation},
            success: function(data) {
                var status = data.status;
                var price_text = $("#not-used-prices").select2('data').text;
                if (status == 'Exists Price'){
                    DX.ui.notification($comments_dic['validate_price_error'], "error");
                    $("#config-zone").css("opacity", 1);
                    $("#formula-text").addClass("error-text");
                    $("#ready-button").removeClass("working");
                    $("#ready-button").text($comments_dic['button_save_message']);
                }
                else{
                   nice_save(price_text);
                }

            }
       });

}

function edit_price_ajax_call(concepts_ids,price,formula,periods,ts,last_importation,last_consolidation){
        $.ajax({
            type: "POST",
            url: '/price_config/' + Pdep_id + "/edit/"+ $price_old,
            cache:false,
            data :{concepts: concepts_ids, price: price, formula: formula, dep_id: Pdep_id, periods:periods, ts:ts, other_concepts:JSON.stringify($json_other_cost),last_importation: last_importation, last_consolidation: last_consolidation},
            success: function(data) {
               var status = data.status;
                var price_text = $("#not-used-prices").select2('data').text;
                if (status == 'Exists Price'){
                    DX.ui.notification("Price has a configuration created already", "error");
                    $("#config-zone").css("opacity", 1);
                    $("#formula-text").addClass("error-text");
                    $("#ready-button").removeClass("working");
                    $("#ready-button").text("Ready! Let's do it");
                }
                else{
                   nice_save(price_text);
                }
            }
       });

}



function check_all(){
    if (!$("#label-checker").hasClass("checked")){
        $('#price .checkbox').each(function() {
            $(this).removeClass('checked');
        });
    }
    else{
        $('#price .checkbox').each(function() {
            $(this).addClass('checked');
        });
    }

}

function check_all_concepts(){
    if (!$("#label-checker-concepts").hasClass("checked")){
        $('#other-concepts-table .checkbox').each(function() {
            $(this).removeClass('checked');
        });
    }
    else{
        $('#other-concepts-table .checkbox').each(function() {
            $(this).addClass('checked');
        });
    }

}

function get_price_config(dep_id, price_id){
    $.ajax({
            type: "GET",
            url: "/price_config/"+dep_id+"/price/"+price_id,
            cache:false,
            dataType: 'json',
            success: function(data) {
                  var price = data.wsm_price;
                  console.log(price)
                  edit_price_config(price)
                }
            })

}

function acceptRecalculationAjaxCall(from_date, to_date, priceid){
    $.ajax({
            type: "POST",
            url: '/list_price/recalculate',
            cache:false,
            data :{dep_id:Pdep_id, price_id: priceid, from_date: from_date, to_date:to_date},
            dataType: 'json',
            success: function(data) {
                  DX.ui.notification($comments_dic['recalculate'],"success");

                }
            })
}

function validateDate(from_date, to_date){
     if (from_date.length == 0 || to_date.length == 0){
         DX.ui.notification($comments_dic['empty_fields_error'], "error");
         return false
     }
    else if ($.datepicker.parseDate('dd/mm/yy',from_date) >= $.datepicker.parseDate('dd/mm/yy',to_date)){
         DX.ui.notification($comments_dic['datepicker_error1'], "error");
         return false
     }
    else if ($.datepicker.parseDate('dd/mm/yy',from_date) < $.datepicker.parseDate('dd/mm/yy','01/01/2014')){
         DX.ui.notification($comments_dic['datepicker_error2'], "error");
         return false
     }
    return true
}

function acceptRecalculation(priceid){
        var from_date = $('#from-date').val();
        var to_date = $('#to-date').val();
        if (validateDate(from_date, to_date)) {
            acceptRecalculationAjaxCall(from_date, to_date, priceid)
            DX.ui.closeAllPopups()
        }

}

function openRecalForm(price){
    DX.ui.popup('popup-recal',$comments_dic['recalculate_tab'])
    $('.ui-dialog').attr('style','display: block; z-index: 1002; outline: 0px; height: auto; width: auto; top: 162px; left: 411px;')
    DX.ui.datepicker('from-date');
    DX.ui.datepicker('to-date');
    $("#ui-datepicker-div").hide();
    var funct = 'acceptRecalculation("'+ price +'")';
    $("#accept-recal").attr('onclick', funct)
}
function convertPriceToTD(jsonPrice){
    var new_row = [];
    new_row.push("<tr>");
    new_row.push('<td style="width:30px;"><label value="'+jsonPrice.price+'" class="checkbox"><input type="checkbox"></label></td>');
    var funct = "get_price_config("+Pdep_id+",'"+jsonPrice.price+"')";
    new_row.push('<td><a href="#" onclick="' + funct +'">' + jsonPrice.price_name + '</a></td>');
    new_row.push("<td>" + jsonPrice.periods + "</td>");
    new_row.push("<td>"+ jsonPrice.last_importation +"</td>");
    new_row.push("<td>"+ jsonPrice.last_consolidation +"</td>");
    var funct2 = 'openRecalForm("'+jsonPrice.price +'")';
    new_row.push("<td><a href='#' onclick='"+ funct2 +"'>"+ $comments_dic['recalculate_tab'] +"</a></td>");
    new_row.push("</tr>");
    return new_row.join()
}

function convertConceptToTD(JsonConcept){
    var new_row = [];
    new_row.push("<tr>");
    new_row.push('<td style="width:30px;"><label value="'+JsonConcept.var+'" class="checkbox"><input type="checkbox"></label></td>');
    var funct = "editOtherConcepts('"+JsonConcept.var+"')";
    new_row.push('<td><a href="#" onclick="' + funct +'">' + JsonConcept.name + '</a></td>');
    new_row.push("<td>" + JsonConcept.var + "</td>");
    new_row.push("</tr>");
    return new_row.join()
}
function load_price_list(page_number,page_prices){
     var table = $("#price tbody");
     table.html("");
    console.log($('.spinner1').html())
    var spinner = '<tr><td></td><td></td><td></td><td>'+ $('.spinner1').html() +'</td><td></td><td></td></tr>'
    console.log(spinner)
    table.append(spinner)
    $.ajax({
            type: "GET",
            url: "/list_price/next_page",
            cache:false,
            data :{page_number: page_number, dep_id:Pdep_id, page_prices:page_prices},
            dataType: 'json',
            success: function(data) {
                var prices_list = data.prices_wsm;
                table.html("");
                $.each(prices_list, function(position) {
                    table.append(convertPriceToTD(prices_list[position]));
                  });
                DX.ui.renderizeCheckboxes();

                }
            })
}
 function deletePriceConf(price_id){
     $.ajax({
            type: "POST",
            url: '/list_price/price/delete',
            cache:false,
            data :{dep_id:Pdep_id, price_id: price_id},
            dataType: 'json',
            success: function(data) {
                  load_page("1", num_pages)
                  DX.ui.notification($comments_dic['delete'],"success");
                }
            })
 }
function load_page(page_number, num_pages){
        var old_page = $("li.active");
        old_page.removeAttr('class');
        var new_page = $("li:contains("+page_number+")");
        new_page.addClass('active');

        if (page_number > 1 && $("#back-page").hasClass('disabled'))
        {
            $("#back-page").removeClass('disabled')
            $("#back-page").attr("onclick", "prev_page()");
        }
        else if(page_number == 1 && !$("#back-page").hasClass('disabled')){
            $("#back-page").addClass('disabled')
            $("#back-page").removeAttr("onclick");
        }

        if (page_number == num_pages && !$("#next-page").hasClass('disabled'))
        {
            $("#next-page").addClass('disabled')
            $("#next-page").removeAttr("onclick");
        }
        else if(page_number < num_pages && $("#next-page").hasClass('disabled')){
            $("#next-page").removeClass('disabled')
            $("#next-page").attr("onclick", "next_page()");
        }

        load_price_list(page_number,page_prices)

    }

function next_page(){
    var old_page = $("li.active").text();
    new_page = parseInt(old_page,10) + 1;
    load_page(new_page.toString(),num_pages)
}
function prev_page(){
    var old_page = $("li.active").text();
    new_page = parseInt(old_page,10) - 1;
    load_page(new_page.toString(),num_pages)
}
function deleteSelectedConcept(){
    $('#price .checkbox').each(function() {
        if ($(this).hasClass('checked')){
            deletePriceConf($(this).attr('value'))
        }
    });

}

function deleteSelectedOtherConcept(){
    $('#other-concepts-table .checkbox').each(function() {
        if ($(this).hasClass('checked')){
            deleteOtherConcept($(this).attr('value'))
        }
    });
}
function deleteOtherConcept(varid){
    var json_object = getOtherConceptConfig(varid);
    //$json_other_cost.remove(json_object)
    $json_other_cost.splice($.inArray(json_object, $json_other_cost),1);
    buildOtherConceptsTable()
}



