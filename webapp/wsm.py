# coding=utf-8

from beaker.middleware import SessionMiddleware
import os

if not os.environ.has_key("SETTINGS_PATH"):
    os.environ["SETTINGS_PATH"] = "/etc/dexma/wsm_webapp.ini"

from src import create_app




session_opts = {
    'session.type': 'file',
    'session.cookie_expires': 6000,
    'session.data_dir': '/var/tmp/',
    'session.auto': True
}

app = create_app()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081, debug=True)
else:
    os.chdir(os.path.dirname(__file__))
    application = app

