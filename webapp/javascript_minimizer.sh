#!/bin/sh
js_files=`find /home/dcortes/PycharmProjects/wsm/webapp/static/js/wsm -name "*.js"`
rm /home/dcortes/PycharmProjects/wsm/webapp/static/js/wsm-compact-min.js
touch /home/dcortes/PycharmProjects/wsm/webapp/static/js/wsm-compact-min.js
for file in $js_files
do
echo "Compressing $file …"
yuicompressor $file > $file.min.js
cat $file.min.js >> /home/dcortes/PycharmProjects/wsm/webapp/static/js/wsm-compact-min.js
rm $file.min.js
done