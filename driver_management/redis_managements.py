__author__ = 'dcortes'

import ConfigParser
import redis
import os
import logging
from logging import NullHandler,StreamHandler
from bson.json_util import loads, dumps
import sys


class redis_management():

    def __init__(self):
        config = self._get_config()
        self._init_redis_db(config)
        self._get_logger()

    def _get_logger(self):
        self.logger = logging.getLogger(self.name)
        if len(self.logger.handlers) == 0:
            self.logger.setLevel(logging.INFO)
            self.handler = StreamHandler(sys.stdout)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def _get_config(self):
        if not os.environ.has_key("SETTINGS_PATH"):
            raise Exception('There is not Setting Path Configuration')
        config_dict = dict()
        config = ConfigParser.RawConfigParser()
        config.read(os.environ["SETTINGS_PATH"])
        config_dict["host"] = config.get("redis", "host")
        config_dict["port"] = config.getint("redis", "port")
        config_dict["db"] = config.getint("redis", "db")
        config_dict["password"] = config.get("redis", "password")
        self.name = config.get("app", "name")
        return config_dict

    def _init_redis_db(self, config):
        try:
            self._db = redis.StrictRedis(host=config['host'], port=config['port'], db=config['db'], password=config['password'],socket_timeout=30,socket_connect_timeout=30)
        except redis.exceptions.ConnectionError, e:
            self.logger.exception("problem with redis")

    def insert_object(self, dep_id, key, value, dumpsState=True):
        try:
            val = dumps(value) if dumpsState else value
            return self._db.set("app:%s:%s:%s" % (self.name, key, dep_id), val)
        except Exception as e:
            self.logger.exception("problem with insert redis")

    def update_object(self, dep_id, key, value):
        try:
            return self._db.set("app:%s:%s:%s" % (self.name, key, dep_id), dumps(value))
        except Exception as e:
            self.logger.exception("problem with updating redis")

    def delete_object(self, dep_id, key):
        try:
            return self._db.delete("app:%s:%s:%s" % (self.name, key, dep_id))
        except Exception as e:
            self.logger.exception("problem with deleting redis")

    def find_object(self, dep_id, key, loads_cond=True):
        try:
            val = self._db.get("app:%s:%s:%s" % (self.name, key, dep_id))
            return loads(val) if loads_cond else val
        except Exception as e:
            self.logger.exception("problem with find redis")

    def find_objects(self, dep_id, key):
        try:
            return loads(self._db.get("app:%s:%s:%s" % (self.name, key, dep_id)))
        except Exception as e:
            self.logger.exception("problem with find various redis")

