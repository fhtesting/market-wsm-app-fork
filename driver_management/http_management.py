__author__ = 'dcortes'

import requests


class http_management():

    @staticmethod
    def call_rest_post(endpoint, url, logger, response="json", headers={}, parameters={}, data={}):
        url = endpoint + url
        logger.info('url:%s' % url)
        try:
            req = requests.post(url, params=parameters, headers=headers, timeout=10, verify=True, data=data)
            if req.status_code in range (200, 299):
                return req.json() if response == "json" else req.content
            else:
                logger.warning("problem with call return %s, message= %s" % (req.status_code,req.text))
        except Exception as e:
            print e
            logger.exception("problem with call")

    @staticmethod
    def call_rest_get(endpoint, url, logger, response="json", headers={}, params={}, stream=False):
        url = endpoint + url
        logger.info('url:%s' % url)
        try:
            req = requests.get(url, params=params, headers=headers, timeout=10, verify=True, stream=stream)
            if req.status_code in range(200, 299):
                return req.json() if response == "json" else req.content
            else:
                logger.warning("problem with call return %s" % req.status_code)
        except Exception as e:
            logger.exception("problem with call")