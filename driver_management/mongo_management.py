__author__ = 'dcortes'

import ConfigParser
from pymongo import MongoClient, errors
import os
import logging
from logging import NullHandler,StreamHandler
import sys

class mongo_management():

    def __init__(self):
        config = self._get_config()
        self._get_logger()
        self._init_mongo_db(config)

    def _get_logger(self):
        self.logger = logging.getLogger(self.name)
        if len(self.logger.handlers) == 0:
            self.logger.setLevel(logging.INFO)
            self.handler = StreamHandler(sys.stdout)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def _get_config(self):
        if not os.environ.has_key("SETTINGS_PATH"):
            raise Exception('There is not Setting Path Configuration')
        config_dict = dict()
        config = ConfigParser.RawConfigParser()
        config.read(os.environ["SETTINGS_PATH"])
        config_dict["host"] = config.get("mongo", "host")
        config_dict["port"] = config.getint("mongo", "port")
        config_dict["user"] = config.get("mongo", "user")
        config_dict["password"] = config.get("mongo", "password")
        config_dict["bd"] = config.get("mongo", "bd")
        config_dict["max_pool_size"] = config.getint("mongo", "max_pool_size")
        config_dict["wait_conn_time"] = config.getint("mongo", "wait_conn_time")
        config_dict["socket_timeout"] = config.getint("mongo", "socket_timeout")
        config_dict["wc_timeout"] = config.getint("mongo", "wc_timeout")
        self.name = config.get("app", "name")
        return config_dict

    def _init_mongo_db(self, config):
        try:
            self._client = MongoClient(config["host"], config["port"], maxPoolSize=config['max_pool_size'],waitQueueTimeoutMS=config['wait_conn_time'],
                                       socketTimeoutMS=config['socket_timeout'],w=1,wtimeout=config['wc_timeout'])
            if self._client[config["bd"]].authenticate(config['user'],config['password']):
                self._db = self._client[config["bd"]]
        except errors.ConnectionFailure, e:
            self.logger.exception("Could not connect to MongoDB: %s" %e)

    def insert_object(self, collection, document):
        try:
            collection = self._db[collection]
            return collection.insert_one(document)
        except Exception as e:
            self.logger.exception("problem with insert to mongodb")

    def update_object(self, collection, document, search_args):
        try:
            collection = self._db[collection]
            return collection.update(search_args, document)
        except Exception as e:
            self.logger.exception("problem with update to mongodb")

    def replace_object(self, collection, document, search_args):
        try:
            collection = self._db[collection]
            return collection.replace_one(search_args, document, upsert=True)
        except Exception as e:
            self.logger.exception("problem with replace to mongodb")

    def delete_object(self, collection, search_args):
        try:
            collection = self._db[collection]
            return collection.remove(search_args, w=1)
        except Exception as e:
            self.logger.exception("problem with delete to mongodb")
            return False

    def find_object(self, collection, search_args):
        try:
            collection = self._db[collection]
            return collection.find_one(search_args, {"_id":0})
        except Exception as e:
            self.logger.exception("problem with find one to mongodb")

    def find_objects(self, collection, search_args, query_args=None,skip=0,limit=0):
        try:
            collection = self._db[collection]
            documents = collection.find(search_args) if query_args is None else collection.find(search_args, query_args,skip=skip,limit=limit)
            return list(documents)
        except Exception as e:
            self.logger.exception("problem with find various to mongodb")

    def count_objects(self, collection, search_args, skip_documents):
            collection = self._db[collection]
            return collection.find(search_args, {"_id":0}, skip=skip_documents).count(True)

    def aggregate_find_object(self, collection, agreggation):
        try:
            collection = self._db[collection]
            return list(collection.aggregate(agreggation))
        except Exception as e:
            self.logger.exception("problem aggregating various to mongodb")

    def get_collections(self):
        try:
            return self._db.collection_names()
        except Exception as e:
            self.logger.exception("problem with find various to mongodb")

    def get_document_exists(self, collection, search_args):
        try:
            ## set count True to make effective first the limit
            ## we use find and not find_one as find just search for a cursor and find_one reads and return the document
            ## so find will be faster
            collection = self._db[collection]
            count = collection.find(filter=search_args).limit(1).count(True)
            return True if count == 1 else False
        except Exception as e:
            self.logger.exception("problem with checking existence of document")