# coding=utf-8
__author__ = 'dcortes'

import ConfigParser
import logging
from logging import NullHandler, StreamHandler
import os
import sys
from bson.json_util import loads
import pytz
import copy
import json

import arrow
from dateutil.relativedelta import relativedelta

from driver_management.redis_managements import redis_management
from driver_management.mongo_management import mongo_management
from driver_management.http_management import http_management

from concepts import CONCEPTS
from datetime import datetime

class Calculator():

    def __init__(self):
        self.config = self._get_config()
        self.manager = mongo_management()
        self._get_logger()

    def _get_logger(self):
        self.logger = logging.getLogger()
        if len(self.logger.handlers) == 0:
            self.logger.setLevel(logging.DEBUG)
            self.handler = StreamHandler(sys.stdout)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def _get_config(self):
        os.environ["SETTINGS_PATH"] = "/etc/dexma/wsm_worker.ini"
        if not os.environ.has_key("SETTINGS_PATH"):
            os.environ["SETTINGS_PATH"] = "/etc/dexma/wsm_worker.ini"
        print os.environ["SETTINGS_PATH"]
        config_dict = dict()
        config = ConfigParser.RawConfigParser()
        config.read(os.environ["SETTINGS_PATH"])
        config_dict["concepts"] = loads(config.get("esios", "concepts"))
        config_dict["concepts_loss"] = config.get("esios", "concepts_loss").split()
        config_dict["url"] = config.get("api", "url")
        self.name = config.get("app", "name")
        self.timezone = config.get("app", "timezone")
        return config_dict

    def _get_variables_formula_concepts(self, formula, concepts):
        concepts_used = list()
        for name in CONCEPTS.keys():
            if name in formula:
                concepts_used.append(CONCEPTS[name])
                formula = formula.replace(name, "v['%s']" % CONCEPTS[name])
        if 'PERD' in formula:
            for loss_concept in self.config["concepts_loss"]:
                if loss_concept in concepts:
                    concepts_used.append(loss_concept)
                    formula = formula.replace('PERD', "v['%s']" % loss_concept)
                    break
        return concepts_used, formula

    def _init_values(self, concepts):
        values = dict()
        for concept in concepts:
            values.update({concept: []})
        return values

    def _get_values(self, from_date, values, concepts):
        month = from_date.strftime("%m-%Y")
        query = {"month": month}
        for concept in concepts:
            doc = self.manager.find_object(concept + "s", query)
            if doc is not None:
                values[concept].extend(doc["values"])
        return values

    def _get_ranged_values(self, from_date, to_date, old_values, concepts, init_values):
        new_values = self._get_values(from_date, init_values, concepts)
        for concept in concepts:
            for val in new_values[concept]:
                ts = arrow.get(val["ts"]).datetime
                if ts >=from_date and ts <= to_date:
                    old_values[concept].append(val)
        return old_values

    """
    def _aggregate_values(self, from_date, to_date, values, concepts):
        pipeline = [
            {"$match": {"month": from_date.strftime("%m-%Y")}},
            {"$unwind": "$values"},
            {"$match": {"values.dt": {"$gte": from_date, "$lte": to_date}}},
            {"$group": {"_id": "$values", "reads": {"$push": '$values'}}},
        ]
        for concept in concepts:
            val = self.manager.aggregate_find_object(concept, pipeline)
            values[concept].extend(val)
        return values
    """

    def _get_concepts_values_mongo(self, concepts_used, from_date, to_date):
        values = self._init_values(concepts_used)
        init_values = copy.deepcopy(values)
        to_month = to_date.replace(day=1, hour=0, minute=0, second=0)
        #Get values when the range date is between  two or more months
        while from_date < to_month:
            values = self._get_values(from_date, values, concepts_used)
            from_date += relativedelta(months=1)
        #return self._aggregate_values(from_date, to_date, values, concepts)
        return self._get_ranged_values(from_date, to_date, values, concepts_used, init_values)

    def _get_variables_formula_others(self, formula, others_concepts):
        concepts_used = list()
        if len(others_concepts):
            vars = [concept["var"] for concept in others_concepts]
            orders = sorted(range(len(vars)), key=lambda k: vars[k]) ## array with the indexes of the ordered array
            orders.reverse()
            for order in orders:
                concept = others_concepts[order]
                var = concept["var"]
                if var in formula:
                    concepts_used.append(concept)
                    formula = formula.replace(concept["var"], "v['%s']" % concept["var"])
        return concepts_used, formula

    def _get_periods_schedule(self, price, from_date, to_date):
        url = "/utility/prices/ELECTRICITY/%s/period-calendar" % price["price"]
        parameters = {"from": from_date.strftime("%Y-%m-%dT%H:%M:%S"), "to": to_date.strftime("%Y-%m-%dT%H:%M:%S")}
        return http_management.call_rest_get(self.config["url"], url, self.logger, headers=self.headers, params=parameters)

    def _order_other_costs_by_time(self, other_costs):
        for pos, cost in enumerate(other_costs):
            if len(cost.get("changeconcepts", [])) > 0:
                changes = cost["changeconcepts"]
                other_costs[pos]["changeconcepts"] = sorted(changes, key=lambda k: k['date'])
        return other_costs

    def _append_periods_to_hash(self, values, reads, other_costs):
        # TODO Add the schedule to add the correct period
        ## hah the concepts time for the ones with changes
        other_costs = self._order_other_costs_by_time(other_costs)
        for read in reads:
            ts = pytz.timezone(self.timezone).localize(datetime.strptime(read["ts"], "%Y-%m-%dT%H:%M:%S")) ## make it fine to convert to a nice datetime without the hour difference
            if ts in values:
                for cost in other_costs:
                    if len(cost["changeconcepts"]) == 0:
                        if ts in values:
                            values[ts][cost["var"]] = cost["concepts"][read["v"]]
                    else:
                        # we need the correct value for that time if the
                        count = -1
                        for change in cost["changeconcepts"]:
                            date_change_concept = pytz.timezone(self.timezone).localize(datetime.strptime(change["date"],"%d/%m/%Y"))
                            if date_change_concept > ts:
                                break
                            count += 1
                        if count == -1:
                            values[ts][cost["var"]] = cost["concepts"][read["v"]]
                        else:
                            values[ts][cost["var"]] = cost["changeconcepts"][count][read["v"]]
        return values

    def _init_hash(self, from_date, to_date):
        values = dict()
        while from_date <= to_date:
            values[from_date] = dict()
            from_date += relativedelta(hours=1)
        return values

    def _get_time_hash(self, concepts_used, from_date, to_date):
        values = self._init_hash(from_date, to_date)
        for concept, reads in concepts_used.items():
            for read in reads:
                try:
                    ts = arrow.get(read["ts"]).datetime
                    values[ts].update({concept: read["v"]})
                except Exception as e:
                    pass
        return values

    def _basic_formula(self, formula, concepts_used, from_date, to_date, len_concepts):
        result = list()
        while from_date <= to_date:
            v = concepts_used[from_date]
            if len(v) == len_concepts:
                try:
                    result.append({
                        "p": (eval(formula)),
                        "ts": from_date.isoformat(),
                    })
                except:
                    print "some problem with the eval"
            from_date += relativedelta(hours=1)
        return result

    def _get_result_formula(self, price, formula, concepts_used, others_used, from_date, to_date, len_concepts):
        values = self._get_time_hash(concepts_used, from_date, to_date)
        if len(others_used) > 0:
            schedule = self._get_periods_schedule(price, from_date, to_date)
            values = self._append_periods_to_hash(values, schedule, others_used)
        return self._basic_formula(formula, values, from_date, to_date, len_concepts + len(others_used))

    def _get_price_values(self, price, from_date, to_date):
        #Getting the concepts used in the formula of the configuration, and the formula with the concepts identificators
        concepts_used, formula = self._get_variables_formula_concepts(price["formula"], price['concepts'])
        #Getting the readings of the different concepts saving in mongodb in date between from date and todate
        values_concepts = self._get_concepts_values_mongo(concepts_used, from_date, to_date)
        #getting if there is any other concept in teh formula, return other concepts and new formula
        others_used, formula = self._get_variables_formula_others(formula, price["other_concepts"])
        return self._get_result_formula(price, formula, values_concepts, others_used, from_date, to_date, len(concepts_used))

    def post_prices(self, price, reads):
        #TODO check that this is the correct one call
        url = "/utility/prices/ELECTRICITY/%s/indexed-rates" % price
        headers = self.headers
        headers["content-type"] = "application/json"
        rest_reading = len(reads)
        init_cell = 0
        while(rest_reading > 0):
            reads_string = json.dumps(reads[init_cell:init_cell+500])
            self.logger.debug(reads_string)
            result = http_management.call_rest_post(self.config["url"], url, self.logger, headers=headers, data=reads_string)
            if result is not None and len(result) == len(reads[init_cell:init_cell+500]):
                init_cell += 500
                rest_reading -= len(result)
            else:
                return False
        return True
    def get_date_in_localtime(self):
        timezone = pytz.all_timezone(self.timezone)
        date_local = timezone.localize(timezone)
        return date_local

    def execution(self, dep_id, price_id, from_date, to_date, task_type):
        self.logger.info('Doing Task from depid = %s, priceid = %s, from date = %s , to date = %s' %(str(dep_id),price_id, from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')))
        from_date_local = pytz.timezone('Europe/Madrid').localize(from_date)
        to_date_local = pytz.timezone('Europe/Madrid').localize(to_date)
        self.logger.debug('Getting Configuration from Mongo')
        price = self.manager.find_object("prices", search_args={"dep_id": dep_id, "price": price_id})
        redis = redis_management()
        self.logger.debug('Getting Token from Redis')
        token = redis.find_object(dep_id, "perm_token", loads_cond = False)
        self.headers = {"x-dexcell-token": token}
        reads = self._get_price_values(price, from_date_local, to_date_local)
        if reads is not None and len(reads) > 0:
            done = self.post_prices(price_id, reads)
            if done:
                self.logger.info('Task from priceid = %s, depid = %s Done' %(str(dep_id),price_id))
                query ={'dep_id': int(dep_id), 'price': price_id}
                last = datetime.now().strftime('%d/%m/%Y %H:%M')
                if task_type == 'importation':
                    update = {"$set":{'last_importation': last}}
                    self.manager.update_object('prices', update, query)
                elif task_type == 'consolidation':
                    update = {'$set':{'last_consolidation': last}}
                    self.manager.update_object('prices', update, query)
            else:
                self.logger.warning('Task from priceid = %s, depid = %s FAIL' %(str(dep_id),price_id))
        else:
            self.logger.warning("No data for this days")

if __name__ == '__main__':
    sc = Calculator()
    sc.execution(179, "5628e902e4b0b9d1c4cd5bbd", datetime.strptime("2017-06-15 00:00:00", "%Y-%m-%d %H:%M:%S"), datetime.strptime("2017-06-30 23:59:59", "%Y-%m-%d %H:%M:%S"),'importation')
