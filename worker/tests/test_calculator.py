__author__ = 'dcortes'

import os
from datetime import datetime

from worker.calculator import Calculator
from testing_conf import conf_path_puller, conf_path_driver


class TestCalculator():

    def setup_method(self, method):
        path = os.path.dirname(os.path.abspath(__file__))
        os.environ["SETTINGS_WORKER_PATH"] = path + "/config.ini"

    def test_get_concepts_formula(self):
        cal = Calculator()
        formula = "sin(CFP) + RPBF -RTR * MI / MI - sqrt(SD)"
        result_concepts, result_formula = cal._get_variables_formula_concepts(formula)
        assert(result_formula == "sin(v['1286']) + v['806'] -v['807'] * v['808'] / v['808'] - sqrt(v['813'])")

    def test_get_basic_formula_result(self):
        cal = Calculator()
        formula = "v['RPBF'] -v['RTR']"
        concepts = {datetime.strptime("01/01/2015 01", "%d/%m/%Y %H"): {"RPBF": 12, "RTR": 7}}
        from_date = datetime.strptime("01/01/2015 01", "%d/%m/%Y %H")
        to_date = datetime.strptime("01/01/2015 01:30", "%d/%m/%Y %H:%M")
        result_concepts = cal._basic_formula(formula, concepts, from_date, to_date, 2)
        assert(result_concepts == [{'p': 5, 'ts': '2015-01-01T01:00:00+01:00'}])

    def test_get_basic_formula_result_some_value_not_present(self):
        cal = Calculator()
        formula = "v['RPBF'] -v['RTR']"
        concepts = {datetime.strptime("01/01/2015 01", "%d/%m/%Y %H"): {"RPBF": 12}}
        from_date = datetime.strptime("01/01/2015 01", "%d/%m/%Y %H")
        to_date = datetime.strptime("01/01/2015 01:30", "%d/%m/%Y %H:%M")
        result_concepts = cal._basic_formula(formula, concepts, from_date, to_date, 2)
        assert(result_concepts == [])
