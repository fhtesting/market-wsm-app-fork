__author__ = 'dcortes'

CONCEPTS = {
    'MD':   '805',
    'RPBF': '806',
    'RTR':  '807',
    'MI':   '808',
    'RI':   '809',
    'RPAS': '810',
    'BS':   '811',
    'DM':   '812',
    'SD':   '813',
    'CFP':  '1286',
    'PC':   '814',
    'SI':   '1277',
    'SPO':  '815',
    'FNUPG':'816',
    'IEB': '1368',
    'PVPC2A': '1013',
    'PVPC2DHA': '1014',
    'PVPC2DHS': '1015'
}