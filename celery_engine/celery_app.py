__author__ = 'dcortes'

from celery import Celery
import os
import ConfigParser

if not os.environ.has_key("SETTINGS_PATH"):
    os.environ["SETTINGS_PATH"] = "/etc/dexma/wsm_worker.ini"

config_dict = dict()
config = ConfigParser.RawConfigParser()
config.read(os.environ["SETTINGS_PATH"])
config_dict["host"] = config.get("broker", "host")
config_dict["broker"] = config.get("broker", "name")
config_dict["password"] = config.get("broker", "password")

app = Celery('proj', broker='%s://:%s@%s//' %(config_dict["broker"], config_dict["password"], config_dict["host"]), include=['celery_engine.tasks'])
app.conf.update(CELERYD_TASK_SOFT_TIME_LIMIT=4000,
                BROKER_POOL_LIMIT=2,
                CELERY_DEFAULT_QUEUE="wsm-celery",
                #CELERY_TASK_SERIALIZER='json',
                CELERYD_PREFETCH_MULTIPLIER=1,
                CELERY_QUEUES = {"wsm-celery": {"exchange": "wsm-celery"}},
                CELERY_ROUTES = {'celery_engine.tasks.calculate_price': {'queue': 'wsm-celery'}},
                CELERYD_TASK_LOG_FORMAT="%(asctime)s - %(levelname)s - %(processName)s - %(name)s - %(module)s - %(funcName)s - %(message)s",
                CELERYD_LOG_LEVEL="DEBUG",
                CELERY_TASK_PUBLISH_RETRY_POLICY={
                    'max_retries': 3,
                    'interval_start': 10, # the moment that will begin
                    'interval_step': 10, # the interval between each retry
                    'interval_max': 10,
                },
                CELERY_ACKS_LATE=True, # as its not possible to know if the workers have finished fine (the broker) we set the ack to make them acknowedlge the end of their task
                BROKER_HEARTBEAT=10, # to check if the connection is still alive
                #BROKER_CONNECTION_RETRY=True, # set like this by default
                BROKER_CONNECTION_MAX_RETRIES=20, # default is set to 100
                BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 300} # The visibility timeout defines the number of seconds to wait for the worker to acknowledge the task before the message is redelivered to another worker
               )


if __name__ == '__main__':
    app.worker_main()